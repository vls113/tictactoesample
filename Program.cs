﻿using System;

namespace TicTacToe
{
    class Program
    {
        static string[,] gameField = { { "*","*","*" },
                                       { "*","*","*" },
                                       { "*","*","*" } };

        static string playerMark = "x";
        static string computerMark = "o";

        static void Main(string[] args)
        {
            //Реализовать игровой цикл до победы
            drawField();
            playerTurn();
            checkWin(playerMark);
            checkWin(computerMark);
        }

        private static void drawField() 
        {
            for (int i = 0; i < 3; i++) 
            {
                for (int j = 0; j < 3; j++) 
                {
                    Console.Write($"| {gameField[i,j]} |");
                }
                Console.WriteLine();
            }
        }

        private static void playerTurn() 
        {
            while (true)
            {
                Console.WriteLine("Введите строку [0,2]: ");
                int i = Convert.ToInt32(Console.ReadLine());

                if (i >= 0 && i < 3)
                {
                    Console.WriteLine("Введите столбец [0,2]: ");
                    int j = Convert.ToInt32(Console.ReadLine());

                    if (j >= 0 && j < 3)
                    {
                        if (gameField[i, j].Equals("*"))
                        {
                            gameField[i, j] = playerMark;
                            drawField();
                            break;
                        }
                        else 
                        {
                            Console.WriteLine("Клетка занята!");
                        }
                    }
                    else 
                    {
                        Console.WriteLine("Вы ввели недупустимое значеине!");
                    }
                }
                else 
                {
                    Console.WriteLine("Вы ввели недупустимое значеине!");
                }                
            }
        }

        private static void computerTurn() 
        { 
            //Рандомим два значения индексов
            //Проверяем, свободна ли клетка
            //Если да - ставим туда "о" и отрисосываем поле
            //Если нет - возвращаемся к пункту 1 
        }

        private static void checkWin(string tag) 
        { 
            //Проверка на заполняемость поля
            /*if(gameField[0,0].Equals(tag) && gameField[0,1].Equals(tag) && gameField[0, 2].Equals(tag) ||
                )*/
        }
    }
}
